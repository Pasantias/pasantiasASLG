<?php

namespace App\Http\Controllers;

use App\Empleados;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmpleadosPostRequest;
use Datatables;

use Redirect;
use Session;

class EmpleadosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('empleados.index');
	}

	public function anyData() {
		return Datatables::of(Empleados::query())
			->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		return view('empleados.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreEmpleadosPostRequest $request) {

		Empleados::create($request->all());

		Session::flash('success', 'Se ha registrado empleado');
		return Redirect::to('empleados');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id_empleado) {
		$empleado = Empleados::find($id_empleado);

		return view('empleados.edit', ['empleado' => $empleado]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(StoreEmpleadosPostRequest $request, $id) {
		$empleado = Empleados::findOrFail($id);
		$empleado->fill($request->all())
			->save();
		Session::flash('success', 'Se ha actualizado informacion del empleado');
		return Redirect::to('empleados');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
