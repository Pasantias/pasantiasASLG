<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
		return view('layouts.inicio');
	});

// ROUTES EMPLEADO
Route::resource('empleados', 'EmpleadosController');
Route::resource('planificacion', 'PlanificacionGralController');

Route::controller('datatables', 'EmpleadosController', [
		'anyData' => 'empleados.anyData',
		'index'   => 'index',
	]);