<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model {
	public $timestamps    = false;
	protected $primaryKey = 'id_empleado';
	protected $fillable   = ['doc_identidad', 'nombres', 'apellidos', 'email', 'genero', 'telefono'];
}
