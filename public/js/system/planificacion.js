
$("#asistentes_internos, #responsable_interno, #objetivo_desemp_id").select2({width: '100%'});
$('#responsable_externo, #asistentes_externos').tokenfield({
        typeahead: [null, { source: engine.ttAdapter() }]

});

function AgregarEvento(){
      event.preventDefault()
       if($('#PlanificacionForm').valid()){
          if($('#asistentes_internos').val()==null &&  $('#asist_ext').val()==''){
            Mensajes('error','El Renglon de Asistentes esta Vacio<br/>Debe completar alguno de los dos campos [internos ó Externos]');
            return;
          }

          if($('#responsable_interno').val()==null &&  $('#responsable_ext').val()==''){
            Mensajes('error','El Renglon de Responsable esta Vacio<br/>Debe completar alguno de los dos campos [internos ó Externos]');
            return;
          }

            $.ajax({
                data:  {
                    'titulo':$('#tituloevento').val(),
                    'descripcion':$('#dscrip_actv').val(),
                    'hora_ini':$('#tiempoinicio').val(),
                    'hora_fin':$('#tiempofin').val(),
                    'objetivo_desemp_id':$('#objetivo_desemp_id').val(),
                    'descrip_actv':$('#dscrip_actv').val(),
                    'asistentes_internos':$('#asistentes_internos').val(),
                    'asistentes_externos':$('#asist_ext').val(),
                    'responsable_interno':$('#responsable_interno').val(),
                    'responsable_externo':$('#responsable_ext').val(),
                    'planificacion_fecha':moment().add(1, 'weeks').weekday($('#dia_semana').val()).format('YYYY-MM-DD'),
                    'empleado_id':1,
                    'lugar':$('#lugar').val(),


                },
                url:   '/sysplanificacion_old/planificacion_gral/addevent',
                type:  'post',
                success:  function (response) {
                    var dat=$.parseJSON(response);
                    if(dat.error==1){
                        Mensajes('error','No se pudo agregar actividad<br/> Error: <b>Coincide con una planificacion previa<b>');
                    }else{
                        LimpiarForm();
                        $('#planificacion').fullCalendar('refetchEvents');
                        Mensajes('success','Se Agregó actividad');
                    }

                },
                error: function (response) {

                }

          });


       }

    }//Fin Agregar Evento

    function EnviarAviso(){

              $.ajax({
                data:  {
                    'titulo_mensaje':'Revisión de Planificación',
                    'remitente_id':1,



                },
                url:   '/sysplanificacion_old/planificacion_gral/updateevent',
                type:  'post',
                success:  function (response) {
                    var dat=$.parseJSON(response);
                    if(dat.error==1){
                        Mensajes('error','No se pudo agregar actividad<br/> Error: <b>Coincide con una planificacion previa<b>');
                    }else{
                        LimpiarForm();
                        $('#planificacion').fullCalendar('refetchEvents');
                        Mensajes('success','Se Actualizó la planificación');
                    }

                },
                error: function (response) {

                }

          });

    }


function NuevoEvento(){
  $('#addevent').show()
  $('#updateevent').hide()
  LimpiarForm();
}

function EditarEvento(){

   event.preventDefault()
       if($('#PlanificacionForm').valid()){
          if($('#asistentes_internos').val()==null &&  $('#asist_ext').val()==''){
            Mensajes('error','El Renglon de Asistentes esta Vacio<br/>Debe completar alguno de los dos campos [internos ó Externos]');
            return;
          }

          if($('#responsable_interno').val()==null &&  $('#responsable_ext').val()==''){
            Mensajes('error','El Renglon de Responsable esta Vacio<br/>Debe completar alguno de los dos campos [internos ó Externos]');
            return;
          }

            $.ajax({
                data:  {
                    'titulo':$('#tituloevento').val(),
                    'descripcion':$('#dscrip_actv').val(),
                    'hora_ini':$('#tiempoinicio').val(),
                    'hora_fin':$('#tiempofin').val(),
                    'descrip_actv':$('#dscrip_actv').val(),
                    'objetivo_desemp_id':$('#objetivo_desemp_id').val(),
                    'asistentes_internos':$('#asistentes_internos').val(),
                    'asistentes_externos':$('#asist_ext').val(),
                    'responsable_interno':$('#responsable_interno').val(),
                    'responsable_externo':$('#responsable_ext').val(),
                    'planificacion_fecha':$('#fech_pla').val(),
                    'empleado_id':1,
                    'id':$('#id_pla').val(),
                    'lugar':$('#lugar').val(),


                },
                url:   '/sysplanificacion_old/planificacion_gral/updateevent',
                type:  'post',
                success:  function (response) {
                    var dat=$.parseJSON(response);
                    if(dat.error==1){
                        Mensajes('error','No se pudo agregar actividad<br/> Error: <b>Coincide con una planificacion previa<b>');
                    }else{
                        LimpiarForm();
                        $('#planificacion').fullCalendar('refetchEvents');
                        Mensajes('success','Se Actualizó la planificación');
                    }

                },
                error: function (response) {

                }

          });


       }

}

function CopiarEvento(){
      event.preventDefault()
       if($('#PlanificacionForm').valid()){
          if($('#asistentes_internos').val()==null &&  $('#asist_ext').val()==''){
            Mensajes('error','El Renglon de Asistentes esta Vacio<br/>Debe completar alguno de los dos campos [internos ó Externos]');
            return;
          }

          if($('#responsable_interno').val()==null &&  $('#responsable_ext').val()==''){
            Mensajes('error','El Renglon de Responsable esta Vacio<br/>Debe completar alguno de los dos campos [internos ó Externos]');
            return;
          }
           semana_start = moment().add(1, 'weeks').weekday(1).format('MMMM DD YYYY');
           semana_end = moment().add(1, 'weeks').weekday(5).format('MMMM DD YYYY');
           $('#semana_pla').val('Del ('+semana_start+') Al ('+semana_end+')');
             $('#addevent').show()
            $('#updateevent').hide()
            $('#deleteevent').hide()
            $('#copyevent').hide()

        }

}

function EliminarEvento(){
   event.preventDefault()
$.ajax({
                data:  {'id':$('#id_pla').val()},
                url:   '/sysplanificacion_old/planificacion_gral/deleteevent',
                type:  'post',
                success:  function (response) {
                    var dat=$.parseJSON(response);
                    if(dat.error==1){
                        Mensajes('error','No se pudo eliminar la actividad');
                    }else{
                        LimpiarForm();
                        $('#id_pla').val('')
                        $('#planificacion').fullCalendar('refetchEvents');
                        Mensajes('success','Se elimino este evento');
                    }

                },
                error: function (response) {

                }

          });

}

$('#planificacion').fullCalendar({
            eventClick: function(calEvent, jsEvent, view) {
                    semana_start = moment(calEvent.fecha_evento).weekday(1).format('MMMM DD YYYY');
                    semana_end = moment(calEvent.fecha_evento).weekday(5).format('MMMM DD YYYY');
                    $('#semana_pla').val('Del ('+semana_start+') Al ('+semana_end+')');
                    $('#tituloevento').val(calEvent.title)
                    $('#dscrip_actv').val(calEvent.descripcion)
                    $('#tiempoinicio').val(calEvent.time_ini)
                    $('#objetivo_desemp_id').select2('val',calEvent.objetivo_desemp_id),
                    $('#tiempofin').val(calEvent.time_fin)
                    $('#asistentes_internos').select2('val',calEvent.asist_inter)
                    $('#asist_ext').tagsinput('removeAll')
                    $('#asist_ext').tagsinput('add', calEvent.asist_exter)
                    $('#responsable_interno').select2('val',calEvent.resp_inter)
                    $('#responsable_ext').tagsinput('removeAll')
                    $('#responsable_ext').tagsinput('add',calEvent.resp_exter)
                    $('#dia_semana').select2('val',moment(calEvent.fecha_evento).weekday())
                    $('#lugar').val(calEvent.lugar)
                    $('#id_pla').val(calEvent.id)
                    $('#fech_pla').val(calEvent.fecha_evento)
                    $('#txtactividad').html('Actualizar Actividad')
                    $('#addevent').hide()
                    if(moment(calEvent.fecha_evento).week()==moment().add(1, 'weeks').week()){
                    $('#updateevent').show()
                    $('#copyevent').show()
                    $('#deleteevent').show()
                    }else if(moment(calEvent.fecha_evento).week()==moment().week()){
                      $('#updateevent').show()
                      $('#copyevent').show()
                      $('#deleteevent').show()
                    }else if(moment(calEvent.fecha_evento).week()<=moment().week()){
                        $('#copyevent').show()
                        $('#addevent').hide()
                        $('#updateevent').hide()
                        $('#deleteevent').hide()
                    }

                    $(this).css('border-color', 'red')

            },
            /*dayClick: function(date, allDay, jsEvent, view) {
              alert('click day');
            },*/
            weekends: false,
            weekMode: 'liquid',

            defaultView: 'agendaWeek',
            minTime:'08:00:00',
            maxTime:'17:00:00',
            allDay: false,
           // theme: true,
      header: {
        left: 'prev,next  today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar
            eventLimit: true,
            events:  '/sysplanificacion_old/planificacion_gral/listaeventos',


    });

$('.fc-left').append('<button type= "button" class="fc-button fc-state-default fc-corner-right fc-corner-left" id="AE_btn_pdf"><i class="fa fa-file-pdf-o"></i> PDF</button>')

/*$('.fc-left').append('<button type= "button" class="fc-button fc-state-default fc-corner-right fc-corner-left" id="Avisar_Mail"><i class="fa fa-at"></i> Enviar</button>')
*/

function download(strData, strFileName, strMimeType) {
    var D = document,
        A = arguments,
        a = D.createElement("a"),
        d = A[0],
        n = A[1],
        t = A[2] || "text/plain";

    //build download link:
    a.href = "data:" + strMimeType + "," + escape(strData);

    if (window.MSBlobBuilder) {
        var bb = new MSBlobBuilder();
        bb.append(strData);
        return navigator.msSaveBlob(bb, strFileName);
    } /* end if(window.MSBlobBuilder) */

    if ('download' in a) {
        a.setAttribute("download", n);
        a.innerHTML = "Descargando...";
        D.body.appendChild(a);
        setTimeout(function() {
            var e = D.createEvent("MouseEvents");
            e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
            D.body.removeChild(a);
        }, 66);
        return true;
    } /* end if('download' in a) */

    //do iframe dataURL download:
    var f = D.createElement("iframe");
    D.body.appendChild(f);
    f.src = 'data:' + (A[2] ? A[2] : 'application/octet-stream') + (window.btoa ? ';base64' : '') + ',' + (window.btoa ? window.btoa : escape)(strData);
    setTimeout(function() {
        D.body.removeChild(f);
    }, 333);
    return true;
}

 $('#AE_btn_pdf').click(function () {
      html2canvas($('.fc-view-container'), {
         logging: true,
         useCORS: false,
         background:'#fff',
         onrendered: function (canvas) {
            var imgData = canvas.toDataURL('image/jpeg');
            var doc = new jsPDF();
            doc.addImage(imgData, 'JPEG', 15, 40, 180, 160);
            download(doc.output(), 'Planificacion.pdf', 'text/pdf');
         }
      }) ;
   });

$('#Avisar_Mail').click(function () {
$('#modal_confirma').modal('show')
   });
