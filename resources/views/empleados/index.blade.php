@extends('layouts.home')

@push('cssTop')
{!! Html::style('plugins/datatables/dataTables.bootstrap.css') !!}
{!! Html::style('plugins/datatables/dataTables.themify.css') !!}
@endpush

@section('content')


<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Listado de Empleados</h2>
                    <div class="panel-ctrls"></div>
                </div>
                <div class="panel-body no-padding">
                    <table id="data_table_empleado" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Documento de Identidad</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Genero</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>
@stop

@push('scriptBottom')
{!! Html::script('plugins/datatables/jquery.dataTables.js') !!}
{!! Html::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script>



base_url='{!! URL::to('/')!!}';
$('#data_table_empleado').dataTable({
        "language": {
            "lengthMenu": "_MENU_"
        },
        processing: true,
        serverSide: true,
        ajax: '{!! route('empleados.anyData') !!}',

        columns: [
            { data: 'doc_identidad', name: 'doc_identidad' },
            { data: 'nombres', name: 'nombres' },
            { data: 'apellidos', name: 'apellidos' },
            { data: 'genero', name: 'genero' },
            { data: 'email', name: 'email' },
            { data: 'telefono', name: 'telefono' },
            {
                "mData": null,
                 "searchable": false,
                 "orderable": false,
                 "visible": true,
                "mRender": function (o) {
                    return '<a  class="tooltips" title="Editar" data-toggle="tooltip" data-placement="top" href='+base_url+"/empleados/"+o.id_empleado+"/edit"+'><span><i class="material-icons">mode_edit</i></span></a>'; }
            }

        ],

        });

    $('.dataTables_filter input').attr('placeholder','Buscar...');


    //DOM Manipulation to move datatable elements integrate to panel
    $('.panel-ctrls').append($('.dataTables_filter').addClass("pull-right")).find("label").addClass("panel-ctrls-center");
    $('.panel-ctrls').append("<i class='separator'></i>");
    $('.panel-ctrls').append($('.dataTables_length').addClass("pull-left")).find("label").addClass("panel-ctrls-center");

    $('.panel-footer').append($(".dataTable+.row"));
    $('.dataTables_paginate>ul.pagination').addClass("pull-right m-n");

</script
@endpush