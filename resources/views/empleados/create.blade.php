@extends('layouts.home')


@section('content')


 <div class="panel panel-midnightblue" data-widget='{"draggable": "false"}'>
                <div class="panel-heading">
                    <h2>Registro de Empleados</h2>
                    <div class="panel-ctrls" data-actions-container="" data-action-collapse='{"target": ".panel-body, .panel-footer"}'></div>
                </div>
                <div class="panel-body">

                {!! Form::open(['route'=>'empleados.store','method' => 'POST','class'=>"form-horizontal row-border", 'id'=>'formempleados','data-parsley-validate']) !!}

                       @include('empleados.forms._form')



                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="btn-toolbar">
                                <button class="btn btn-raised btn-info">Registrar</button>
                                <button class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>




@stop