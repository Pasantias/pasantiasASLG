            <div class="form-group">
                            {!! csrf_field() !!}
                            {!!Form::label('Documento de Identidad',null,['class'=>'col-sm-2 control-label'])!!}
                            <div class="col-sm-6">
                                {!! Form::text('doc_identidad',null,['placeholder'=>'e.g V-01.234.567', 'required', 'class'=>"form-control mask", 'data-inputmask'=>" 'mask':'99.999.999'"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('nombres',null,['class'=>'col-sm-2 control-label'])!!}
                            <div class="col-sm-6">
                                {!!Form::text('nombres',null,['placeholder'=>'e.g pedro pablo', 'required', 'class'=>"form-control"])!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('apellidos',null,['class'=>'col-sm-2 control-label'])!!}
                            <div class="col-sm-6">
                                {!!Form::text('apellidos',null,['placeholder'=>'e.g perez prado', 'required', 'class'=>"form-control"])!!}
                            </div>
                        </div>
                        <div class="form-group">
                             {!!Form::label('Genero',null,['class'=>'col-sm-2 control-label'])!!}
                            <div class="col-sm-8">
                                <div class="radio">
                                            <label>{!!Form::radio('genero', 'M')!!} Masculino</label>
                                            <label>{!!Form::radio('genero', 'F')!!} Femenino</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('Correo Electronico',null,['class'=>'col-sm-2 control-label'])!!}
                            <div class="col-sm-6">
                                {!!Form::email('email',null,['placeholder'=>'e.g usuario@servidor.dominio', 'required', 'class'=>"form-control"])!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('Telefono',null,['class'=>'col-sm-2 control-label'])!!}
                            <div class="col-sm-6">
                                {!!Form::text('telefono',null,['placeholder'=>'e.g (0000)000.00.00', 'required', 'class'=>"form-control"])!!}
                            </div>
                        </div>


@push('scriptBottom')
{!! Html::script('plugins/form-inputmask/jquery.inputmask.bundle.min.js') !!}
@endpush