@extends('layouts.home')

@push('cssTop')
{!! Html::style('plugins/fullcalendar/fullcalendar.css') !!}
{!! Html::style('plugins/form-select2/select2.css') !!}
{!! Html::style('plugins/bootstrap-tokenfield/css/bootstrap-tokenfield.css') !!}

@endpush


@section('content')

<div class="col-md-5">
<div class="panel panel-teal" data-widget='{"draggable": "false"}'>
    <div class="panel-heading">
       <h2><div id="txtactividad">Cargar Actividad</div></h2>
         <div class="panel-controls dropdown" data-actions-container="" data-action-collapse='{"target": ".panel-body, .panel-footer"}'>
                                            <button class="btn btn-icon-rounded" onClick="LimpiarForm()" data-toggle="tooltip" data-placement="top" title="Nueva Actividad"><span class="material-icons inverted">refresh</span></button>
          </div>
    </div>
    <div class="panel-editbox" data-widget-controls=""></div>

                                <div class="panel-body">
                                  <form id="PlanificacionForm"  class="form-horizontal form-validation">
                          <br/>

                              <div class="form-group">
                                    <label for="objetivo">Objetivo de desempeño</label>
                                   <select name="objetivo_desemp_id" id="objetivo_desemp_id"  required="required" style="width:100% !important" class="populate" >
                                                                                      <option value="1">Prueba Objetivo 1</option>
                                                                                     <option value="2">Prueba Objetivo 2</option>
                                                                                     <option value="3">Prueba Objetivo 3</option>
                                                                                    </select>

                              </div>


                              <div class="form-group">
                                    <label for="titulo">Titulo Actividad/Evento</label>
                                    <input type="text" name="tituloevento" id="tituloevento" required="required" class="form-control col-md-6"  placeholder="e.g Reunion con productores" maxlength="25" id="titulo">
                                    <input type="hidden" name="id_pla" id="id_pla">
                                    <input type="hidden" name="fech_pla" id="fech_pla">
                              </div>

                              <div class="form-group col-md-12">
                                        <label for="semana" class="col-sm-2 control-label">semana</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" readonly name="semana_pla" id="semana_pla">
                                        </div>
                              </div>

                          <div class="row">
                              <div class="form-group col-md-12">
                                     <label for="dia" class="col-sm-2 control-label">Dia</label>
                                          <div class="col-sm-8">
                                          <select name="dia_semana"  required="required" id="dia_semana" class="form-control js-states"  required style="width: 100%" >
                                            <option value="1">Lunes</option>
                                            <option value="2">Martes</option>
                                            <option value="3">Miercoles</option>
                                            <option value="4">Jueves</option>
                                            <option value="5">Viernes</option>
                                          </select>
                                        </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="form-group col-md-12">
                                        <label class="col-sm-2 control-label">Lugar</label>
                                        <div class="col-md-10">
                                              <input type="text" required class="form-control col-md-12" name="lugar" id="lugar" />
                                        </div>
                              </div>
                          </div>

                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Hora Inicio</label>
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                       <div class='input-group date' id='timeStart'>
                                                            <input type='text' required class="form-control" name="tiempoinicio" id="tiempoinicio" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                   <label class="col-sm-2 control-label">Hora Fin</label>
                                                    <div class="col-md-5">
                                                        <div class='input-group date' id='timeEnd'>
                                                            <input type='text' required class="form-control" name="tiempofin" id="tiempofin" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                            </div>

                             <div class="form-group">
                                            <label class="col-sm-2 control-label">Asitentes Internos</label>
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                       <select multiple style="width:100% !important" class="populate" id="asistentes_internos" >
                                                                                                                <option value="1">Dervins Maswer Guapes Brito [18943376]</option>
                                                                                                                <option value="6">Yulianna Martinez [1234567899]</option>
                                                                                                            </select>
                                                    </div>
                                                   <label class="col-sm-2 control-label">Asistentes Externos</label>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control" name="asist_ext" id="asist_ext" data-role="tagsinput" >
                                                    </div>
                                                </div>
                                            </div>
                            </div>
                            <div class="form-group">
                                            <label class="col-sm-2 control-label">Responsables Internos</label>
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                       <select class="populate"  multiple="multiple"   style="width:100% !important" name="responsable_interno" id="responsable_interno" >
                                                                                                                <option value="1">Dervins Maswer Guapes Brito [18943376]</option>
                                                                                                                <option value="6">Yulianna Martinez [1234567899]</option>
                                                                                                            </select>
                                                    </div>
                                                   <label class="col-sm-2 control-label">Responsables Externos</label>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control" name="responsable_ext" id="responsable_ext"  >
                                                    </div>
                                                </div>
                                            </div>
                            </div>

                          <div class="row">
                              <div class="form-group col-md-12">
                                        <label class="col-sm-2 control-label">Actividad</label>
                                        <div class="col-sm-10">
                                              <textarea required class="col-sm-10" row="20" name="dscrip_actv" id="dscrip_actv"></textarea>
                                        </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button onClick="EliminarEvento()" id="deleteevent" style="display: none;" class="btn  btn-danger btn-rounded">Eliminar</button>
                              <button onClick="CopiarEvento()" id="copyevent" style="display: none;" class="btn btn-info btn-rounded">Copiar</button>
                              <button onClick="AgregarEvento()" id="addevent" class="btn btn-success btn-rounded">Cargar</button>
                              <button onClick="EditarEvento()" id="updateevent" style="display: none;" class="btn btn-success btn-rounded">Actualizar</button>
                          </div>
                  </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div id="planificacion"></div>
                                </div>
                            </div>
                        </div>


 <div class="modal fade bs-example-modal-sm" id="modal_confirma" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Confirmar Envío de Aviso</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            ¿Esta usted seguro(a) de enviar un aviso a su superior indicando que ha cargado toda su planificación?
                                                            <br/>
                                                            <b>Recuerde que despues de enviar el correo no podra hacer ninguna modificacion hasta tanto su superior apruebe las modificaciones en su planificación</b>
                                                            <br/>
                                                            ¿Esta de Acuerdo?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                            <button type="button" class="btn btn-success" onclick="EnviarAviso()">Si</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>










</div>
                    </div><!-- Termina el cuerpo del las vistas-->

@stop


@push('scriptBottom')
{!! Html::script('plugins/fullcalendar/moment.min.js') !!}
{!! Html::script('plugins/fullcalendar/fullcalendar.min.js') !!}
{!! Html::script('plugins/jsPDF/jspdf.js') !!}
{!! Html::script('js/html2canvas.js') !!}
{!! Html::script('plugins/jsPDF/jspdf.plugin.addimage.js') !!}
{!! Html::script('plugins/form-select2/select2.min.js ') !!}
{!! Html::script('plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js') !!}
{!! Html::script('js/system/planificacion.js') !!}
@endpush