<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sistema de Planificación, Control y Seguimiento - UT Guarico</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="Gurico, Planificacion control, Seguimiento">
    <meta name="author" content="UT-GUARICO">
    <link rel="shortcut icon" href="assets/img/logo-icon-dark.png">

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500' rel='stylesheet'>
    <link type='text/css'  href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">

    {!! Html::style('fonts/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('css/styles.css') !!}
    {!! Html::style('plugins/codeprettifier/prettify.css') !!}
    {!! Html::style('plugins/dropdown.js/jquery.dropdown.css') !!}
    {!! Html::style('plugins/progress-skylo/skylo.css') !!}
    {!! Html::style('plugins/pines-notify/pnotify.css') !!}
    {!! Html::style('plugins/snackbar/snackbar.min.css') !!}
    <!--[if lt IE 10]>
        <script src="assets/js/media.match.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <script src="assets/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->


    @stack('cssTop')

    </head>

    <body class="animated-content infobar-overlay">


<header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner">


    <div class="logo-area">
        <a class="navbar-brand navbar-brand-primary" href="index.html">
            <img class="show-on-collapse img-logo-white" alt="Paper" src="assets/img/logo-icon-white.png">
            <img class="show-on-collapse img-logo-dark" alt="Paper" src="assets/img/logo-icon-dark.png">
            <img class="img-white" alt="Paper" src="assets/img/logo-white.png">
            <img class="img-dark" alt="Paper" src="assets/img/logo-dark.png">
        </a>

        <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg stay-on-search">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                <span class="icon-bg">
                    <i class="material-icons">menu</i>
                </span>
            </a>
        </span>
        <span id="trigger-search" class="toolbar-trigger toolbar-icon-bg ov-h">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                <span class="icon-bg">
                    <i class="material-icons">search</i>
                </span>
            </a>
        </span>
        <div id="search-box">
            <input class="form-control" type="text" placeholder="Search..." id="search-input"></input>
        </div>
    </div><!-- logo-area -->

    <ul class="nav navbar-nav toolbar pull-right">

        <li class="toolbar-icon-bg appear-on-search ov-h" id="trigger-search-close">
            <a class="toggle-fullscreen"><span class="icon-bg">
                <i class="material-icons">close</i>
            </span></i></a>
        </li>
        <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
            <a href="#" class="toggle-fullscreen"><span class="icon-bg">
                <i class="material-icons">fullscreen</i>
            </span></i></a>
        </li>





    </ul>

</header>

        <div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-gray">
                    <div class="static-sidebar">
                        <div class="sidebar">
    <div class="widget" id="widget-profileinfo">
        <div class="widget-body">
            <div class="userinfo ">
                <div class="avatar pull-left">
                    <img src="assets/demo/avatar/avatar_15.png" class="img-responsive img-circle">
                </div>
                <div class="info">
                    <span class="username">Nombres</span>
                    <span class="useremail">Cargo</span>
                </div>

                <div class="acct-dropdown clearfix dropdown">
                    <span class="pull-left"><span class="online-status online"></span>Online</span>
                </div>
            </div>
        </div>
    </div>
    <div class="widget stay-on-collapse" id="widget-sidebar">
<nav role="navigation" class="widget-body">
    <ul class="acc-menu">
        <li class="nav-separator"><span>Menú</span></li>
        <h4>
       &nbsp;&nbsp;&nbsp;&nbsp;<a  class="withripple" >    
        <i class="material-icons">home</i>{!! link_to('/', 'Inicio',[],null) !!}</a> 
        </h4>


        <li><a  class="withripple" href="javascript:;"><span class="icon"><i class="material-icons">person</i></span><span>Empleados</span></a>
            <ul class="acc-menu">
                <li>{!! link_to_action('EmpleadosController@create','Registrar', [], ['class'=>'withripple'])  !!}</li>
                <li>{!! link_to_action('EmpleadosController@index','Lista', [], ['class'=>'withripple'])  !!}</li>
            </ul>
        </li>

        <li><a  class="withripple" href="javascript:;"><span class="icon"><i class="
         material-icons">folder</i></span><span>Planificar</span></a>
            <ul class="acc-menu">
            
                <li>{!! link_to_action('PlanificacionGralController@index','Planificacion', [], ['class'=>'withripple'])  !!}</li>
            </ul>
        </li>


    </ul>
</nav>
    </div>
</div>
                    </div>
                </div>
<div class="static-content-wrapper">
    <div class="static-content">
        <div class="page-content">
            <ol class="breadcrumb">
                <li class="">{!! link_to('/', 'Inicio',[],null) !!}</li>
                <li class="active"><a href="index.html">Dashboard</a></li>
            </ol>
            <div class="page-heading">
                <h1>Dashboard<small>Project Statistics</small></h1>

            </div>
            <div class="container-fluid">
                <!---CONTENT-->
                 @if(Session::has('success'))
                        @push('messagesBottom')
<script>
                        messages_system('success','{{ Session::get('success') }}')
</script>
                        @endpush
                @endif
                        <div class="row">
                            <div class="col-xs-12">
                                @yield('content')
                            </div>
                        </div>
                <!-- END CONTENT-->
            </div>
        </div> <!-- .container-fluid -->
    </div> <!-- #page-content -->


    <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;">Fundación para el Desarrollo de la Ciencia y la Tecnología en el Estado Guarico</h6></li>
                </ul>
            </div>
    </footer>

</div>
</div>
</div>


{!! Html::script('js/jquery-1.10.2.min.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}
{!! Html::script('js/enquire.min.js') !!}
{!! Html::script('plugins/velocityjs/velocity.min.js') !!}
{!! Html::script('plugins/velocityjs/velocity.ui.min.js') !!}
{!! Html::script('plugins/progress-skylo/skylo.js') !!}
{!! Html::script('plugins/wijets/wijets.js') !!}

{!! Html::script('plugins/codeprettifier/prettify.js') !!}
{!! Html::script('plugins/nanoScroller/js/jquery.nanoscroller.min.js') !!}
{!! Html::script('plugins/dropdown.js/jquery.dropdown.js') !!}
{!! Html::script('plugins/bootstrap-material-design/js/material.min.js') !!}
{!! Html::script('plugins/bootstrap-material-design/js/ripples.min.js') !!}
{!! Html::script('plugins/pines-notify/pnotify.min.js') !!}
{!! Html::script('plugins/snackbar/snackbar.min.js') !!}
{!! Html::script('demo/demo.js') !!}
{!! Html::script('js/application.js') !!}
@stack('messagesBottom')
@stack('scriptBottom')

    </body>
</html>