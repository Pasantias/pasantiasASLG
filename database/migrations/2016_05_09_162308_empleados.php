<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Empleados extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('empleados', function (Blueprint $table) {
				$table->increments('id_empleado');
				$table->string('doc_identidad');
				$table->string('nombres');
				$table->string('apellidos');
				$table->char('genero', 1);
				$table->string('email');
				$table->string('telefono');
				//$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
